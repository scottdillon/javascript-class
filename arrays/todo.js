//Create array with 5 different to dos
// print a message that includes the length
// print the first tand second to last items

const todos = [{
    text: 'get milk',
    completed: true
}, {
    text: 'get bananas',
    completed: false
}, {
    text: 'get cereal',
    completed: true
}, {
    text: 'get paper',
    completed: false
}, {
    text: 'get paid',
    completed: true
}]

// console.log(`You have ${todos.length} to dos.`)
// console.log(`The first to-do is '${todos[0]}'`)
// console.log(`the second to-do is '${todos[todos.length - 2]}'.`)

// todos.splice(2, 1)
// todos.push('New Items to do')
// todos.shift()

// 1. first item
// 2. 2nd item

// todos.forEach(function (item, index) {
//     console.log(`${index + 1}. ${item}`)
// })

// for (let index = 0; index < todos.length; index++) {
//     const todo = todos[index];
//     console.log(`${index + 1}. ${todo}`)

// }

// Convert array of string to objects -> text, completed
// Create function to remove a todo by text value

// Finding an object and removing it from the array
const deleteTodo = function (todos, todoTitle) {
    const index = todos.findIndex(function (todo, index) {
        return todo.text.toLowerCase() === todoTitle.toLowerCase()
    })
    console.log(index)
    if (index > -1) {
        todos.splice(index, 1)
    }

}

// deleteTodo(todos, 'Get Cereal')
// console.log(todos)

const getThingsToDo = function (todos) {
    return todos.filter(function (todo, index) {
        return !todo.completed
    })
}

// console.log(getThingsToDo(todos))
const sortToDos = function (todos) {
    todos.sort(function (a, b) {
        if (a.completed < b.completed) {
            return -1
        } else if (b.completed < a.completed) {
            return 1
        } else {
            return 0
        }
    })
}

sortToDos(todos)

console.log(todos)
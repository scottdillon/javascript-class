const account = {
    name: 'Scott Dillon',
    expenses: [],
    income: [],
    addExpense: function (description, amount) {
        this.expenses.push({
            description: description,
            amount: amount
        })
    },
    addIncome: function (description, amount) {
        this.income.push({
            description: description,
            amount: amount
        })
    },
    getAccountSummary: function () {
        let totalExpenses = 0
        let totalIncome = 0
        this.expenses.forEach(function (expense) {
            totalExpenses = totalExpenses + expense.amount
        })
        this.income.forEach(function (income) {
            totalIncome = totalIncome + income.amount
        })
        return `${this.name} has a balance of $${totalIncome - totalExpenses}. $${totalIncome} in income. $${totalExpenses} in expenses.`
    }
}


// Expense -> description (string), amount (number)

// addExpense -> description, amount
// adds a new object to thte expenses array


// getAccountSummary -> total up all expenses -> Scott Dillon has $1250 in expenses.

account.addExpense('Rent', 950)
account.addExpense('Coffee', 2)
account.addIncome('Job', 1000)
console.log(account)
console.log(account.getAccountSummary())

// add IncomeArray to account
// addIncome method -> description, amount
// Tweak getAccountSummary -> Scott Dillon has a balance of $10. $100 in income. $90 in expenses.
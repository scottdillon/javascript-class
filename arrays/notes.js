// const notes = ['Note 1', 'Note 2', 'Note 3']
const notes = [{
    title: 'My Next Trip',
    body: 'I would like to go to Spain'
}, {
    title: 'Habits to work on',
    body: 'Exercise. Eating Better.'
}, {
    title: 'Office Modifications',
    body: 'Get a new seat'
}]

// notes.push('My new note')
// console.log(notes.length)

// console.log(notes[0])

// console.log(notes.pop())
// console.log(notes.length)

// console.log(notes.shift())
// console.log(`After shifting we get: ${notes}`)

// console.log(notes.unshift('New first note'))
// console.log(notes)


// notes.splice(1, 1)
// console.log(notes.length)
// console.log(notes)

// notes.splice(1, 0, 'this is the new 2nd item')

// notes.forEach(function (item, index) {
//     console.log(index)
//     console.log(item)
// })


// console.log(notes.length)
// console.log(notes)

// for (let index = 0; index < notes.length; index++) {
//     const note = notes[index];
//     console.log(index + note)
// }

// for (let count = 0; count < notes.length; count++) {
//     const element = notes[count];

// }

// // Searching arrays
// console.log(notes.indexOf('Note 2'))

// let someObject = {}
// let otherObject = someObject
// console.log(someObject === otherObject)

// const index = notes.findIndex(function (note, index) {
//     console.log(note)
//     return note.title === 'Habits to work on'
// })
// console.log(index)

// const findNote = function (notes, noteTitle) {
//     const index = notes.findIndex(function (note, index) {
//         return note.title.toLowerCase() === noteTitle.toLowerCase()
//     })
//     return notes[index]
// }


// const findNote = function (notes, noteTitle) {
//     const note = notes.find(function (note, index) {
//         return note.title.toLowerCase() === noteTitle.toLowerCase()
//     })
//     return note
// }


// const searchNotes = function () {

// }

// const note = findNote(notes, 'Office Modifications')
// console.log(note)

// const findNotes = function (notes, query) {
//     return notes.filter(function (note, index) {
//         const isTitleMatch = note.title.toLowerCase().includes(query.toLowerCase())
//         const isBodyMatch = note.body.toLowerCase().includes(query.toLowerCase())
//         return isTitleMatch || isBodyMatch
//     })
// }

// console.log(findNotes(notes, 'eating'))


// Sorting
const sortNotes = function (notes) {
    notes.sort(function (a, b) {
        if (a.title.toLowerCase() < b.title.toLowerCase()) {
            return -1
        } else if (b.title.toLowerCase() < a.title.toLowerCase()) {
            return 1
        } else {
            return 0
        }
    })
}

sortNotes(notes)

console.log(notes)
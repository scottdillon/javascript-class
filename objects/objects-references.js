let myAccount = {
    name: 'Scott Dillon',
    expenses: 0,
    income: 0
}

let addExpense = function (account, amount) {
    account.expenses = account.expenses + amount
}

// addIncome
let addIncome = function (account, income) {
    account.income = account.income + income
}

// resetAccount reset to zero
let resetAccount = function (account) {
    account.income = 0
    account.expenses = 0
}

// getAccountSummary
// Account for Andrew has $900, $1000 in income. $100 in expenses.
let getAccountSummary = function (account) {
    console.log(`Account for ${account.name} has $${account.income - account.expenses}. $${account.income} in income.  $${account.expenses} in expenses.`)
}

addIncome(myAccount, 1000)
addExpense(myAccount, 100)
addExpense(myAccount, 500)
getAccountSummary(myAccount)
resetAccount(myAccount)
getAccountSummary(myAccount)
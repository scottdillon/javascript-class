let num = 103.941

// results in: 103.94
console.log(num.toFixed(2))

// results in: 103.9
console.log(num.toFixed(1))

// results in: 104
console.log(num.toFixed(0))

// results in: 103.941000000000003
console.log(num.toFixed(15))

// results in Error
// console.log(num.toFixed(-1))

// results in: undefined
console.log(num.EPSILON)
console.log(num.isNaN)
console.log(num.NaN)

console.log(Math.round(num))
console.log(Math.floor(num))
console.log(Math.ceil(num))

console.log(Math.random())

let min = 10
let max = 20
let randomNum = Math.floor(Math.random() * (max - min + 1)) + min

console.log(randomNum)

// Challenge
// range 1-5, true if correct, false if not correct

let makeGuess = function (guess) {
    let max = 5
    let min = 1
    let roll = Math.floor(Math.random() * (max - min + 1)) + min
    console.log(roll)
    return roll === guess
}

console.log(makeGuess(1))
console.log(makeGuess(1))
console.log(makeGuess(1))
console.log(makeGuess(1))
console.log(makeGuess(1))
console.log(makeGuess(1))
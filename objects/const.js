let isRaining = true
isRaining = false
console.log(isRaining)

const isSnowing = true
// isSnowing = false

console.log(isSnowing)

const person = {
    age: 27
}

console.log(person)
person.age = 30

console.log(person)


// redeclare these vars with const where applicable
const gradeCalc = function (score, total) {
    const percentScore = score / total * 100
    let letterGrade = null
    if (percentScore >= 90) {
        letterGrade = 'A'
    } else if (percentScore >= 80) {
        letterGrade = 'B'
    } else if (percentScore >= 70) {
        letterGrade = 'C'
    } else if (percentScore >= 60) {
        letterGrade = 'D'
    } else {
        letterGrade = 'F'
    }
    return `You got a ${letterGrade} (${percentScore}%)!`
}

console.log(gradeCalc(15, 20))
console.log(gradeCalc(0, 100))
console.log(gradeCalc(99, 100))
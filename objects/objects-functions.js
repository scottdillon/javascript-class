let myBook = {
    title: '1984',
    author: 'George Orwell',
    pageCount: 326
}

let otherBook = {
    title: 'A Peoples History of the United States',
    author: 'Howard Zim',
    pageCount: 723
}

let getSummary = function (book) {
    return {
        summary: `${book.title} by ${book.author}`,
        pageCountSummary: `${book.title} is ${book.pageCount} pages long`
    }

}

let bookSummary = getSummary(myBook)
let otherBookSummary = getSummary(otherBook)

console.log(bookSummary.pageCountSummary)

// Challenge Area

let convertFahrenheitToCelsius = function (temperature) {
    return {
        fahrenheit: temperature,
        celsius: (temperature - 32) * 5 / 9,
        kelvin: (temperature - (32 + 459.67)) * 5 / 9
    }
}

console.log(convertFahrenheitToCelsius(0))
console.log(convertFahrenheitToCelsius(100))
console.log(convertFahrenheitToCelsius(32))
console.log(convertFahrenheitToCelsius(75))
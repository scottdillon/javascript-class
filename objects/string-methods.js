let name = '   Scott Dillon   '

// length property
console.log(name.length)

// Convert to Uppercase
console.log(name.toUpperCase())

console.log(name.toLowerCase())

// Include method
let password = 'abc123password098'
console.log(password.includes('password'))

// Trim
console.log(name.trim())



let isValidPassword = function (testPW) {
    return testPW.length > 8 && !testPW.includes('password')
}

console.log(isValidPassword('asdfp'))
console.log(isValidPassword('abc123!@#$%^&*'))
console.log(isValidPassword('asdfpasdpoijpassword'))
let myBook = {
    title: '1984',
    author: 'George Orwell',
    pageCount: 326
}

console.log(myBook.title)
console.log(`${myBook.title} by ${myBook.author}`)

myBook.title = 'Animal Farm'

console.log(`${myBook.title} by ${myBook.author}`)

// Challenge - model a person
// name, age, location

let person = {
    name: 'Scott Dillon',
    age: 43,
    location: 'Morrisville'
}

console.log(`${person.name} is ${person.age} and lives in ${person.location}.`)
person.age = person.age - 24
console.log(`${person.name} is ${person.age} and lives in ${person.location}.`)
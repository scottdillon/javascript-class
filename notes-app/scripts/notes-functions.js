'use strict'

// read existing notes from localstorage
const getSavedNotes = () => {
    const NotesJSON = localStorage.getItem('notes')
    try {
        return NotesJSON ? JSON.parse(NotesJSON) : []
    } catch (e) {
        return []
    }
}

// Generate the DOM structure for a note
const gnerateNoteDom = (note, index) => {
    const newNoteDiv = document.createElement('a')
    const textEl = document.createElement('p')
    const statusEl = document.createElement('p')
    // Setup the remove note button
    // const button = document.createElement('button')
    // button.textContent = 'x'
    // newNoteDiv.appendChild(button)
    // button.addEventListener('click', (e) => {
    //     removeNote(note.id)
    //     saveNotes()
    //     renderNotes(notes, filters)

    // })

    if (note.title.length > 0) {
        textEl.textContent = note.title
    } else {
        textEl.textContent = `Title: ${note.title}`
    }
    textEl.classList.add('list-item__title')
    // textEl.href = `/edit.html#${note.id}`

    newNoteDiv.appendChild(textEl)
    newNoteDiv.href = `/edit.html#${note.id}`
    newNoteDiv.classList.add('list-item')
    statusEl.textContent = generateLastEdited(note.updatedAt)
    statusEl.classList.add('list-item__subtitle')
    newNoteDiv.appendChild(statusEl)

    return newNoteDiv
}

// sort your notes by one of three ways
const sortNotes = (notes, sortBy) => {
    if (sortBy === 'byEdited') {
        return notes.sort((a, b) => {
            if (a.updatedAt > b.updatedAt) {
                return -1
            } else if (a.updatedAt < b.updatedAt) {
                return 1
            } else {
                return 0
            }
        })
    } else if (sortBy === 'byCreated') {
        return notes.sort((a, b) => {
            if (a.createdAt > b.createdAt) {
                return -1
            } else if (a.createdAt < b.createdAt) {
                return 1
            } else {
                return 0
            }
        })
    } else if (sortBy === 'alphabetical') {
        return notes.sort((a, b) => {
            if (a.title.toLowerCase() < b.title.toLowerCase()) {
                return -1
            } else if (a.title.toLowerCase() > b.title.toLowerCase()) {
                return 1
            } else {
                return 0
            }
        })
    } else {
        return notes
    }
}

// render application notes
const renderNotes = (notes, filter) => {
    notes = sortNotes(notes, filters.sortBy)
    const filteredNotes = notes.filter((note) => note.title.toLowerCase().includes(filter.searchText.toLowerCase()))
    const notesElement = document.querySelector('#notebook')
    notesElement.innerHTML = ''

    if (filteredNotes.length > 0) {
        filteredNotes.forEach((note, index) => {
            const newNoteEl = gnerateNoteDom(note, index)
            notesElement.appendChild(newNoteEl)
        })
    } else {
        const emptyMessage = document.createElement('p')
        emptyMessage.classList.add('empty-message')
        emptyMessage.textContent = 'No notes to show'
        notesElement.appendChild(emptyMessage)
    }

}

// save note sto localStorage
const saveNotes = (notes) => {
    localStorage.setItem('notes', JSON.stringify(notes))
}

// remove a note from the list
const removeNote = (id) => {
    const noteIndex = notes.findIndex((note) => note.id === id)
    if (noteIndex > -1) {
        notes.splice(noteIndex, 1)
    }
}

const generateLastEdited = (timestamp) => `Last edited ${moment(timestamp).fromNow()}`
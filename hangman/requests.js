const getPuzzle = async (wordCount) => {
    const response = await fetch(`http://puzzle.mead.io/puzzle?wordCount=${wordCount}`)

    if (response.status === 200) {
        const data = await response.json()
        return data.puzzle
    } else {
        throw new Error('Unable to get puzzle')
    }
}

const getCountry = async (code) => {
    const response = await fetch('http://restcountries.eu/rest/v2/all')
    if (response.status === 200) {
        const data = await response.json()
        return data.find((country) => country.alpha2Code === code)
    } else {
        throw new Error(`Unable to fetch the country data with error code ${response.status}`)
    }
}

const getLocation = async (token) => {
    const url = `https://ipinfo.io/json?token=${token}`
    const response = await fetch(url)
    if (response.status === 200) {
        const data = await response.json()
        return data
    } else {
        throw new Error(`Unable to make request. Status code: ${response.status}`)
    }
}

const token = '1c77e3aef847a1'
const getCurrentCountry = async () => {
    const location = await getLocation(token)
    return getCountry(location.country)
}


// getCurrentCountry().then((country) => {
//     console.log(`Country name is: ${country.name}`)
// }).catch((error) => {
//     console.log(error)
// })
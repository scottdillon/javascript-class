'use strict'
// HTTP --> request --> response
const setupKeypressListen = (gameInstance) => {
    window.addEventListener('keypress', (e) => {
        const guess = String.fromCharCode(e.charCode)
        gameInstance.makeGuess(guess)
        updatePuzzle(gameInstance)
        updateMessage(gameInstance)
    })
}

const updatePuzzle = (hm) => {
    const puzzleElement = document.querySelector('#puzzle')
    puzzleElement.textContent = hm.puzzle
}
const updateMessage = (hm) => {
    const guessesElement = document.querySelector('#guess-left')
    guessesElement.textContent = hm.statusMessage
}

// const wordCount = '3'
// getPuzzle(wordCount).then((puzzle) => {
//     console.log(puzzle)
// }).catch((err) => {
//     console.log(`Error: ${err}`)
// })

const main = async () => {
    const phrase = await getPuzzle(5)
    const hm = new Hangman(phrase, 5)
    setupKeypressListen(hm)
    updatePuzzle(hm)
}
main()

document.querySelector('#reset').addEventListener('click', main)
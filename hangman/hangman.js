class Hangman {
    constructor(word, guesses) {
        this.status = 'playing'
        this.secretWord = word.toLowerCase().split('')
        this.guesses = guesses
        this.guessedLetters = []
    }
    get puzzle() {
        let mask = this.secretWord.map((letter) => {
            return this.guessedLetters.includes(letter) || letter === ' ' ? letter : '*'
        })
        return mask.join('')
    }

    makeGuess(guess) {
        if (this.status === 'playing') {
            guess = guess.toLowerCase()
            if (!this.guessedLetters.includes(guess)) {
                this.guessedLetters.push(guess)
                if (!this.secretWord.includes(guess)) {
                    this.guesses-- //-= 1
                }
            }
            this.changeStatus()
        }
    }

    isPuzzleComplete() {
        return this.secretWord.every((puzzleLetter) => {
            return this.guessedLetters.includes(puzzleLetter) || puzzleLetter == ' '
        })
    }

    changeStatus() {
        const completed = this.isPuzzleComplete()
        const outOfGuesses = this.guesses < 1
        if (!completed && outOfGuesses) {
            this.status = 'failed'
        } else if (completed && !outOfGuesses) {
            this.status = 'finished'
        } else if (!completed && !outOfGuesses) {
            this.status = 'playing'
        }
    }

    get statusMessage() {
        if (this.status === 'playing') {
            return `Guesses left: ${this.guesses}`
        } else if (this.status.toLowerCase() === 'failed') {
            return `Nice try! The word was "${ this.secretWord.join('')}".`
        } else if (this.status.toLowerCase() === 'finished') {
            return `Great work! You guessed the word.`
        }
    }
}
FROM node:lts-alpine

ENV PATH "$PATH:/usr/local/lib/node_modules/npm/node_modules/.bin"
WORKDIR /app

COPY package*.json ./

RUN npm install -g live-server
RUN npm install && \
    npm install moment

COPY . .

EXPOSE 8080

CMD ["/bin/bash"]

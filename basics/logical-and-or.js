let temp = 55

// Logical and operator - True if both sides are true. False otherwise.

// Logical  Or operator = True if at least one side is true, False otherwise.
if (temp >= 60 && temp <= 90) {
    console.log('It it pretty nice out')
} else if (temp <= 0 || temp >= 120) {
    console.log('Do not go outside')
} else {
    console.log('Eh, do what you want.')
}


// Challenge

let isGuestOneVegan = true
let isGuestTwoVegan = true

// Are both vegan? Only offer vegan dishes
// At least one guest vegan? Make sure to offer some vegan options.
// Neither guest vegan? Offer up anything on the menu.

if (isGuestOneVegan && isGuestTwoVegan) {
    console.log("Offer only vegan dishes")
} else if (isGuestOneVegan || isGuestTwoVegan) {
    console.log("Make sure to offer some vegan options")
} else {
    console.log("OFfer anything on the menu.")
}
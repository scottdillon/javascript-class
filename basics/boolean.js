// === is the equality operator
let temp = 112
let isFreezing = temp === 32
console.log(isFreezing)
// !== - no equality operator
isFreezing = temp !== 32
console.log(isFreezing)
// < - less than operator
isFreezing = temp < 32
console.log(isFreezing)

// > - greater than
isFreezing = temp > 32
console.log(isFreezing)

// >= - greater than or equal to
isFreezing = temp >= 32
console.log(isFreezing)

// <= - less than or equal to
// isFreezing = 

if (temp <= 32) {
    console.log('It is freezing!')
}

if (temp >= 110) {
    console.log('is is way too hot outside')
    console.log('testing 123')
}

// 7 or under is child
// 65 or older is senion
// print isChild value
// print isSenior value
let age = 6

// if 7 or under print msg about child pricing
if (age <= 7) {
    console.log("You have child pricing")
}

if (age >= 65) {
    console.log("Youre old")
}

// if 65 or older, print senior message 
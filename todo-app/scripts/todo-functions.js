'use strict'

// Fetch existing todos from localStorage
const getSavedTodos = () => {
    const toDosJSON = localStorage.getItem('todos')
    try {
        return toDosJSON ? JSON.parse(toDosJSON) : []
    } catch {
        return []
    }
}

// Save todos to localStorage
const saveTodos = (todos) => localStorage.setItem('todos', JSON.stringify(todos))

const applyCheckboxChange = (id, state) => {
    const todo = todos.find((todo) => todo.id === id)

    if (todo) {
        todo.completed = !todo.completed
    }
}

const generageTodoDOM = (todo, index) => {
    const todoRoot = document.createElement('label')
    const containerEl = document.createElement('div')
    const checkbox = document.createElement('input')
    const newToDo = document.createElement('span')
    const deleteButton = document.createElement('button')

    // Setup todo checkbox
    // checkbox.type = 'checkbox'
    checkbox.setAttribute('type', 'checkbox')
    checkbox.checked = todo.completed
    containerEl.appendChild(checkbox)
    checkbox.addEventListener('change', (e) => {
        applyCheckboxChange(todo.id, e.target.checked)
        saveTodos(todos)
        renderToDos(todos, filters)
    })

    // setup todo text element
    newToDo.textContent = `${todo.text}`
    containerEl.appendChild(newToDo)
    todoRoot.classList.add('list-item')
    containerEl.classList.add('list-item__container')
    todoRoot.appendChild(containerEl)
    // setup delete button
    deleteButton.textContent = 'remove'
    deleteButton.classList.add('button', 'button--text')
    todoRoot.appendChild(deleteButton)
    deleteButton.addEventListener('click', (e) => {
        removeTodo(todo.id)
        saveTodos(todos)
        renderToDos(todos, filters)
    })

    // setup container

    return todoRoot
}

const generateSummaryDOM = (filteredTasks) => {
    const unfinishedToDos = filteredTasks.filter((todo) => !todo.completed)

    const newSummary = document.createElement('h2')
    newSummary.classList.add('list-title')
    const todosPlural = unfinishedToDos.length === 1 ? 'todo' : 'todos'
    newSummary.textContent = `You have ${unfinishedToDos.length} ${todosPlural} left.`
    return newSummary
}

// Render application todos based on filters
const renderToDos = (todoTasks, filters) => {
    let filteredTasks = todoTasks.filter((todo) => {
        const searchTextMatch = todo.text.toLowerCase().includes(filters.searchText.toLowerCase())
        const hideCompletedMatch = !filters.hideComplete || !todo.completed
        return searchTextMatch && hideCompletedMatch
    })
    const todoHolder = document.querySelector('#todo-holder')
    todoHolder.innerHTML = ''

    todoHolder.appendChild(generateSummaryDOM(filteredTasks))

    if (filteredTasks.length > 0) {
        filteredTasks.forEach((todo, index) => {
            todoHolder.appendChild(generageTodoDOM(todo, index))
        })
    } else {
        const emptyMessage = document.createElement('p')
        emptyMessage.textContent = 'No todos to show'
        emptyMessage.classList.add('empty-message')
        todoHolder.appendChild(emptyMessage)
    }

}

const removeTodo = (id) => {
    const todoIndex = todos.findIndex((todo) => todo.id === id)

    if (todoIndex > -1) {
        todos.splice(todoIndex, 1)
    }
}
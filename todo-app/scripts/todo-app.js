'use strict'

let todos = getSavedTodos()

const filters = {
    searchText: '',
    hideComplete: false
}

renderToDos(todos, filters)

document.querySelector('#search-text').addEventListener('input', (e) => {
    filters.searchText = e.target.value
    renderToDos(todos, filters)
})

document.querySelector('#todoForm').addEventListener('submit', (e) => {
    e.preventDefault(e.target)
    const inputString = e.target.elements.todoText.value.trim()
    if (inputString.length > 0) {
        todos.push({
            id: uuidv4(),
            text: inputString,
            completed: false
        })
        saveTodos(todos)
        e.target.elements.todoText.value = ''
        renderToDos(todos, filters)
    }

})

document.querySelector('#hide-completed').addEventListener('change', (e) => {
    filters.hideComplete = e.target.checked
    renderToDos(todos, filters)
})
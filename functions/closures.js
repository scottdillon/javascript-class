const createCounter = () => {
    let count = 0

    return {
        increment() {
            count++
        },
        decrement() {
            count--
        },
        get() {
            return count
        }
    }
}

const counter = createCounter()
counter.increment()
counter.decrement()
counter.decrement()
console.log(counter.get())

// Adder

const createAdder = (a) => {
    return (b) => {
        return a + b
    }
}
const add10 = createAdder(10)
console.log(add10(-2))
console.log(add10(20))

// Tipper

const createTipper = (tip) => {
    return (amount) => {
        return amount * tip
    }
}

const tip15Percent = createTipper(0.15)
const tip20Percent = createTipper(0.2)
const tip25Percent = createTipper(0.25)

console.log(tip15Percent(100))
console.log(tip20Percent(100))
console.log(tip25Percent(100))
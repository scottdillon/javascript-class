// Multiple ARguments
let add = function (a, b, c) {
    return a + b + c
}

let result = add(10, 1, 5)
console.log(result)


// Default arguments
let getScoreText = function (name = 'Anonymous', score = 0) {
    // return 'Name: ' + name + ' - Score: ' + score
    return `Name: ${name} - Score: ${score}`
}

let scoreText = getScoreText('Scott', 100)
console.log(scoreText)

// To skip passing in the first argument, use undefined keyword.
let newScoreText = getScoreText(undefined, 100)
console.log(newScoreText)

// Challenge area
// total, tipPercent

// Challenge 2: // A 25% tip on $40 would be $10
let getTip = function (ttl, tPercent = 0.2) {
    return `A ${ 100 * tPercent }% tip on $${total} would be $${total * tPercent}`
}

let tipPercent
let total = 100

let tip1 = getTip(total, tipPercent)
// console.log(tip1)
console.log(tip1)

tipPercent = 0.5
total = 50
let tip2 = getTip(total, tipPercent)
// console.log(tip2)
console.log(tip2)

tipPercent = 0.1
total = 100
let tip3 = getTip(total, tipPercent)
// console.log(tip3)
console.log(tip3)
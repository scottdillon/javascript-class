let name

name = 'Jen'

if (name === undefined) {
    console.log("please provide a name")
} else {
    console.log(name)
}

// Undefined for funtion arguments

let square = function (num) {
    console.log(num)
}

let result = square(4)

console.log(result)

let age = 27
console.log(age)
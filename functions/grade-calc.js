// students score, total possible score
// 10/20 = 75%
// You got a C (75%)!

let gradeCalc = function (score, total) {
    if (typeof score !== 'number' || typeof total !== 'number') {
        throw Error('Please provide numebrs only')
    }

    const percentScore = score / total * 100
    let letterGrade = null
    if (percentScore >= 90) {
        letterGrade = 'A'
    } else if (percentScore >= 80) {
        letterGrade = 'B'
    } else if (percentScore >= 70) {
        letterGrade = 'C'
    } else if (percentScore >= 60) {
        letterGrade = 'D'
    } else {
        letterGrade = 'F'
    }
    return `You got a ${letterGrade} (${percentScore}%)!`
}

try {
    console.log(gradeCalc(15, 25))
    console.log(gradeCalc(0, 100))
    console.log(gradeCalc(99, 100))
} catch (e) {
    console.log(e.message)
}
// Function - input, code, output

let greetUser = function () {
    console.log('Welcome User')
}

greetUser()
greetUser()
greetUser()

let square = function (number) {
    let result = number * number
    return result
}

let num = 3
let retVal = square(num)
let retVal2 = square(8)
console.log(retVal)
console.log(retVal2)
console.log(square(5))
console.log(square(2))

// convertFahrenheitToCelsius

let convertFahrenheitToCelsius = function (temperature) {
    return (temperature - 32) * 5 / 9
}

let temp1 = convertFahrenheitToCelsius(32)
let temp2 = convertFahrenheitToCelsius(68)

console.log(temp1)
console.log(temp2)
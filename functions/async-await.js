const getDataPromise = (data) => new Promise((resolve, reject) => {
    setTimeout(() => {
        typeof data === 'number' ? resolve(data * 2) : reject('Number must be provided')
    }, 500)
})


const processData = async () => {
    let data = await getDataPromise(2)
    data = await getDataPromise(data)
    data = await getDataPromise(data)
    return data
}

processData().then((data) => {
    console.log('Data:', data)
}).catch((error) => {
    console.log('Error', error)
})
const products = ['some item']
const product = products[0]

// truthy - Values that resolve to true in a boolean context
// Falsey - Values that resolve to false in a boolean context
// Falsey = false, 0, '', null, undefined, NaN

if ('testing') {
    console.log('product found')
} else {
    console.log('product not found')
}

// if (product !== undefined) {
//     console.log('product found')
// } else {
//     console.log('product not found')
// }
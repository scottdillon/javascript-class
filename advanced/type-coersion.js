//type coercion - a string, a number, a boolean

console.log('5' + 5)
console.log('5' - 5)

console.log(5 == 5) // true
console.log('5' == 5) //true always use ===
console.log('5' === 5) //false 

console.log(typeof 123) // number
console.log(typeof '123') // string
console.log(typeof [])

const value = true + 12

const type = typeof value
console.log(type)
console.log(value)
// const myAge = 13
// // let message
// const message = myAge >= 18 ? 'you can vote' : 'you can\'t vote'
// // message = myAge >= 18 ? 'you can vote' : 'you can\'t vote'
// console.log(message)

const myAge = 19
const showPage = () => {
    return 'Showing the page'
}
const showErrorPage = () => {
    return 'Showing the error page'
}

const message = myAge >= 21 ? showPage() : showErrorPage()
console.log(message)

const team = ['Tyler', 'Porter', 'clarence', 'clearance', 'too big']

const m2 = team.length <= 4 ? `Team size: ${team.length}` : 'Too many people on your team.'
console.log(m2)
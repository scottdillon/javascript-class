const square = (x) => x * x

console.log(square(5))


const squareLong = (x) => {
    return x * x
}

const people = [{
        name: 'Scott',
        age: 28
    },
    {
        name: 'Vikram',
        age: 31
    }, {
        name: 'Jess',
        age: 22
    }
]

// const under30 = people.filter(function (person) {
//     return person.age < 30
// })

// console.log(under30)

const under30 = people.filter((person) => person.age < 30)

console.log(under30)

// find person with age == 22

const findAge = people.find((p) => p.age === 22)

console.log(findAge.name)
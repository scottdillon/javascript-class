'use strict'
class Person {
    constructor(firstName, lastName, age, likes = []) {
        this.firstName = firstName
        this.lastName = lastName
        this.age = age
        this.likes = likes
    }
    getBio() {
        let bio = `${this.firstName} is ${this.age}.`
        this.likes.forEach((like) => {
            bio += ` ${this.firstName} likes ${like}.`
        })
        return bio
    }
    setName(fullName) {
        const names = fullName.split(' ')
        this.firstName = names[0]
        this.lastName = names[1]
    }
}

class Student extends Person {
    constructor(firstName, lastName, age, likes = [], grade) {
        super(firstName, lastName, age, likes)
        this.grade = grade
        this.status = this.grade >= 70 ? 'passing' : 'failing'
    }
    updateStatus() {
        this.status = this.grade >= 70 ? 'passing' : 'failing'
    }
    updateGrade(extraCredit) {
        this.grade = this.grade + extraCredit
        this.updateStatus()
    }
    getBio() {
        let bio = `${this.firstName} is ${ this.status} the class.`
        return bio
    }

}

const me = new Person('Scott', 'Dillon', 40, ['teaching', 'biking'])

me.setName('David Jason')
console.log(me.getBio())
const person2 = new Person('Clancey', 'Turner', 51)
console.log(person2.getBio())

const scott = new Student('scott', 'dillon', 40, [], 99)
console.log(scott.getBio())

scott.updateGrade(-50)
console.log(scott.getBio())
# Javascript Course

These are notes to accompany taking the [Udemy.com course on javascript by Andrew Mead](https://www.udemy.com/modern-javascript/learn/v4/t/lecture/8913650?start=0)

## Run Node
Node can be run from the terminal via
```
docker run -it --name js-node -v /Users/sdillon/Learning_Projects/javascript_course:/usr/src/app -w /usr/src/app node:8.15-jessie /bin/bash
```

